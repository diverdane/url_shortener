VERSION                 := $(git describe --always --tags --dirty | sed "s/\(.*\)-g`git rev-parse --short HEAD`/\1/")
REGISTRY  		        := dockerhub.com
PUBLIC_ORG              := diverdane
IMAGE_NAME               = url-shortener
REPOSITORY               = ${REGISTRY}/${PUBLIC_ORG}/${IMAGE_NAME}
IMAGE_FULL_PATH          = ${REPOSITORY}:${VERSION}

SHORTENER_DIR         := $(shell pwd)
SHORTENER_BUILD_DIR   := build
SHORTENER_BIN_DIR     := ${SHORTENER_BUILD_DIR}/_output/bin
SHORTENER_REPO_PATH   := bitbucket.org/url_shortener
SHORTENER_BUILD_PATH  := ${SHORTENER_REPO_PATH}/cmd/url_shortener
PATH_TO_SHORTENER_SRC := $$GOPATH/src/bitbucket.org/url_shortener

.PHONY: build

shortener: build_shortener image_shortener
clean:
	go clean
	rm -f ${SHORTENER_BIN_DIR}/url-shortener-*

build_shortener:  fmt vet
	cd ${PATH_TO_SHORTENER_SRC}; mkdir -p ${SHORTENER_BIN_DIR}; \
	#GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ${SHORTENER_BIN_DIR}/${IMAGE_NAME}-linux-amd64 ${SHORTENER_BUILD_PATH}
	go build -o ${SHORTENER_BIN_DIR}/${IMAGE_NAME} ${SHORTENER_BUILD_PATH}

build:  build_shortener

image_shortener:
	# TBD

image: image_shortener

push_shortener_image:
	# TBD

push_image: push_shortener_image

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

