package server

import (
	sql "database/sql"
	"fmt"

	// Blank import is used since only initialization is required for mysql.
	_ "github.com/go-sql-driver/mysql"
)

const (
	databaseName   = "url_shortener"
	tableName      = "url_map"
	longURLColumn  = "long_url"
	shortURLColumn = "short_url"
)

// URLDatabase implements a short-to-long URL mapping database.
// It is exported solely so that the caller can do a defer on the
// Close() method after creating the database.
type URLDatabase struct {
	sqlDB *sql.DB
	Close func() error
}

var urlDB *URLDatabase

// CreateDatabase creates the short-to-long URL mapping database
func CreateDatabase() (*URLDatabase, error) {
	// Connect to mySQL server
	sqlDB, err := mysqlConnect()
	if err != nil {
		return nil, err
	}
	urlDB = &URLDatabase{}
	urlDB.sqlDB = sqlDB
	urlDB.Close = sqlDB.Close

	// Create the database if not already created.
	// Fatally exits upon error.
	urlDB.createURLDatabase()

	// Create the URL mapping table if necessary.
	// Fatally exits upon error.
	urlDB.createTable()

	return urlDB, nil
}

func mysqlConnect() (*sql.DB, error) {
	// TODO: username/password from file system. Ultimately, the file system
	// entries should be set via a Kubernetes secret so that they are not
	// hardcoded here.
	return sql.Open("mysql", "root:P@ssw0rd@tcp(localhost:3306)/")
	// It's expected that the caller will use 'defer sqlDB.Close()'
}

// createURLDatabase creates a URL shortener database if not already created.
func (db *URLDatabase) createURLDatabase() {
	fmt.Printf("Creating database %s if not already created\n", databaseName)
	cmd := "CREATE DATABASE IF NOT EXISTS " + databaseName
	_, err := db.sqlDB.Exec(cmd)
	checkErr(err)
	fmt.Printf("Using database %s\n", databaseName)
	_, err = db.sqlDB.Exec("USE " + databaseName)
	checkErr(err)
}

// createTable creates a URL shortener database if not already created.
func (db *URLDatabase) createTable() {
	fmt.Printf("Creating table %s if not already created\n", tableName)
	rowSchema := fmt.Sprintf("`%s` TEXT, `%s` TEXT", longURLColumn, shortURLColumn)
	cmd := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` ( %s )", tableName, rowSchema)
	_, err := db.sqlDB.Exec(cmd)
	if err != nil {
		err = fmt.Errorf("error executing '%s', error: %s", cmd, err)
		panic(err)
	}
}

func (db *URLDatabase) getShortURL(longURL string) (string, error) {
	_, err := db.sqlDB.Exec("USE " + databaseName)
	if err != nil {
		return "", err
	}
	fmt.Printf("Searching for short URL entry for long URL '%s'\n", longURL)
	cmd := fmt.Sprintf("SELECT %s FROM %s WHERE %s = '%s'", shortURLColumn, tableName, longURLColumn, longURL)
	rows, err := db.sqlDB.Query(cmd)
	if err != nil {
		return "", fmt.Errorf("error executing '%s', error: %s", cmd, err)
	}
	var url string
	for rows.Next() {
		if err = rows.Scan(&url); err != nil {
			return "", fmt.Errorf("error reading short URL for '%s', error: %s", longURL, err)
		}
		break
	}
	fmt.Printf("Found short URL '%s' for long URL '%s'\n", url, longURL)
	return url, nil
}

func (db *URLDatabase) getLongURL(shortURL string) (string, error) {
	_, err := db.sqlDB.Exec("USE " + databaseName)
	if err != nil {
		return "", err
	}
	fmt.Printf("Searching for long URL for short URL '%s'\n", shortURL)
	cmd := fmt.Sprintf("SELECT %s FROM %s WHERE %s = '%s'", longURLColumn, tableName, shortURLColumn, shortURL)
	rows, err := db.sqlDB.Query(cmd)
	if err != nil {
		return "", fmt.Errorf("error executing '%s', error: %s", cmd, err)
	}
	var longURL string
	for rows.Next() {
		if err = rows.Scan(&longURL); err != nil {
			return "", fmt.Errorf("error reading long URL for '%s', error: %s", shortURL, err)
		}
		break
	}
	return longURL, nil
}

func (db *URLDatabase) insertMapping(longURL, shortURL string) error {
	fmt.Printf("Inserting short URL entry %s for long URL '%s'\n", shortURL, longURL)
	cmd := fmt.Sprintf("INSERT INTO %s(%s,%s) VALUES('%s', '%s')", tableName, longURLColumn, shortURLColumn, longURL, shortURL)
	res, err := db.sqlDB.Exec(cmd)
	if err != nil {
		return fmt.Errorf("error executing '%s', error: %s", cmd, err)
	}
	_, err = res.LastInsertId()
	if err != nil {
		return fmt.Errorf("error inserting into URL map with cmd '%s', error: %s", cmd, err)
	}
	return nil
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
