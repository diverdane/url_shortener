package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type route struct {
	method     string
	path       string
	pathPrefix string
	handler    http.HandlerFunc
}

// routes defines HTTP endpoints and their handlers
var routes = []route{
	{
		method:  "POST",
		path:    "/shorten",
		handler: createShortURL,
	},
	{
		method:     "GET",
		pathPrefix: "/expand/",
		handler:    redirectToLongURL,
	},
}

// NewRouter creates a URL router based on routes defined above.
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		fmt.Printf("Adding route: %v\n", route)
		if route.pathPrefix == "" {
			router.
				Methods(route.method).
				Path(route.path).
				Handler(route.handler)
		} else {
			router.
				Methods(route.method).
				PathPrefix(route.pathPrefix).
				//Handler(route.handler)
				Handler(http.StripPrefix(route.pathPrefix, route.handler))
		}
	}

	return router
}
