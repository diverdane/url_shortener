package server

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	urlShortenerAPI "bitbucket.org/url_shortener/api"
)

const (
	shortURLLen        = 7
	maxURLLen          = 2048
	urlUniquenessTries = 1000
)

var (
	// Alphabet for generating base 62 short URLs
	alphabet = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

// URLMap is the data model for the URL shortener state
//type URLMap struct {
//	ShortURL string `json:"shortURL,omitempty"`
//	LongURL  string `json:"longURL,omitempty"`
//}

// InitURLGen initializes the random seed for generating short URLs
func InitURLGen() {
	rand.Seed(time.Now().UnixNano())
}

// JSONResponse represents the body returned for an API POST request
//type JSONResponse struct {
//	Code   int    `json:"code"`
//	Msg    string `json:"msg"`
//	URLMap URLMap `json:"urlMap"`
//}

// NOTE: Manually test the createShortURL with a curl command such as the
// following:
//       curl -H "Content-Type: application/json" -d '{"longURL":"http://foo.example.com"}' http://localhost:8080/shorten

func createShortURL(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Request: %v\n", r)
	// Read body of request to get long URL
	var urlMap urlShortenerAPI.URLMap
	_ = json.NewDecoder(r.Body).Decode(&urlMap)
	longURL := urlMap.LongURL

	// Check for invalid URL being passed.
	if longURL == "" {
		http.Error(w, "null URL", http.StatusBadRequest)
		return
	}
	if len(longURL) > maxURLLen {
		http.Error(w, "url is too long", http.StatusBadRequest)
		return
	}

	// If there is no short URL entry for this long URL in the database,
	// generate a new short URL. Otherwise, return the existing short URL
	// for this long URL.
	shortURL, err := urlDB.getShortURL(longURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if shortURL == "" {
		fmt.Printf("short URL doesn't exist, generating new short URL for %s\n", longURL)
		var tries int
		for tries = 0; tries < urlUniquenessTries; tries++ {
			u := make([]rune, shortURLLen)
			for i := range u {
				u[i] = alphabet[rand.Intn(len(alphabet))]
			}
			shortURL = string(u)

			// Make sure that short URL is unique
			longURL, err := urlDB.getLongURL(shortURL)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			if longURL != "" {
				// Generated URL is not unique. Try again.
				fmt.Printf("Generated short URL '%s' is not unique. Retrying.\n", shortURL)
				continue
			}

			// Write the new short-to-long URL mapping to the database
			fmt.Printf("Generated unique short URL '%s'\n", shortURL)
			if err = urlDB.insertMapping(longURL, shortURL); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		if tries > urlUniquenessTries {
			errMsg := "too many attempts to generate a unique short URL"
			http.Error(w, errMsg, http.StatusInternalServerError)
		}
	}

	// Respond with the URL mapping
	urlMap.ShortURL = shortURL
	urlMapResponse(w, urlMap)
	return
}

// NOTE: Manually test the redirectToLongURL with a curl command such as the
// following:
//    curl -H "Content-Type: application/json" http://localhost:8080/expand/ncPncgU

func redirectToLongURL(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Request: %v\n", r)
	fmt.Printf("URL: %v\n", r.URL)
	fmt.Printf("Path: %v\n", r.URL.Path)

	shortURL := r.URL.Path
	if len(shortURL) != shortURLLen {
		errMsg := fmt.Sprintf("invalid URL '%s'", shortURL)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	// Check to see if there is a long URL entry for this short
	// URL in the database.
	longURL, err := urlDB.getLongURL(shortURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if longURL == "" {
		// URL is not in the database. Return 404 error.
		http.NotFound(w, r)
		return
	}
	// Redirect to the long URL.
	fmt.Printf("Long URL: %s\n", longURL)
	http.Redirect(w, r, longURL, http.StatusMovedPermanently)
}

// TODO: Add DELETE handler

func urlMapResponse(w http.ResponseWriter, urlMap urlShortenerAPI.URLMap) {
	status := http.StatusOK
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	resp := urlShortenerAPI.JSONResponse{
		Code:   status,
		URLMap: urlMap,
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		panic(err)
	}
}
