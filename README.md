# URL Shortener

The URL Shortener is a service that can take a long URL and map that to a more convenient short URL. The short URL can then be used to access the original target of the long URL via a redirect provided by the URL shortener server.

The URL Shortener implementation comprises two components:

1. A RESTful web service that allows users to:
- POST a URL and receive a short URL in return, or...
- GET a short URL and be redirected to the original long URL.
2. TBD: A web app for users to access the service's features.

## Requirements
The URL Shortener service requires a mySQL service running at localhost:3306 on the host on which the URL shortener is running.

## Installation

### Getting the source code

The source code for this repository can be cloned with the following command:
```bash
git clone https://diverdane@bitbucket.org/diverdane/url_shortener.git
```

### Building the Binaries
TBD

### Building Docker Images
TBD

#### Build Requirements:
TBD

## Testing with curl Commands
Until a web app is created to test the URL shortener service, the service can be tested manually using curl commands.

For example, to generate a shortened URL:
```bash
curl -H "Content-Type: application/json" -d '{"longURL":"https://example.com"}' http://localhost:8080/shorten
```

And to use a shortened URL to access the original long-URL target:
```bash
curl -H "Content-Type: application/json" http://localhost:8080/expand/ncPncgU
```

## API

This design uses the following JSON structures as responses to POST REST calls:

```bash
// URLMap represents a short-to-long URL mapping
type URLMap struct {
	ShortURL string `json:"shortURL,omitempty"`
	LongURL  string `json:"longURL,omitempty"`
}

// JSONResponse represents the body returned for an API POST request
type JSONResponse struct {
	Code       int    `json:"code"`
	Msg        string `json:"msg"`
	APIVersion string `json:"apiVersion"`
	URLMap     URLMap `json:"urlMap"`
}
```

## Design Overview
- Long-to-Short URL mappings are maintained in a mySQL persistent database.
- Short URLs are 7-character, Base-62 encoded random alphanumeric strings (including characters 0-9, a-z, and A-Z).
- Long URLs and short URLs maintained in the database must be unique in order to maintain consistent URL mappings.
- Uniqueness of short URLs is maintained by verifying that any generated random short URL does not already exist in the database. If there is a collision (short URL is already being used), the server will keep trying to generate new random short URLs until a unique URL is find, for up to a maximum of 100 tries.
- The URL mapping database is created on demand, if it doesn't already exist.

## TODO / Wish List

Given more time, here are a list if things that I would consider adding to this design:

- REST Client / Web App for using this service.
- Aging out of URL mappings, e.g. delete mappings after 30 days.
- API versioning.
- Add UT
- Add CI
- Eliminate hard-coded mySQL credentials. Instead, read the credentials out of file system space so that the credentials can eventually be populated via a Kubernetes secret when the service is deployed in a Kubernetes platform.
- Add Go mod support for dependencies
- Move database support to a separate package
- Create an interface for database support so that different database implementations (other than mySQL) can be used.
- Convert fmt.Printf calls to logging calls.
- Consider adding token or session state support to provide privacy.
