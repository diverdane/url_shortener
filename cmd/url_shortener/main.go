package main

import (
	"log"
	"net/http"
	"time"

	"bitbucket.org/url_shortener/pkg/server"
)

const (
	serverAddr        = "127.0.0.1:8080"
	serverTimeoutSecs = 15
)

func main() {
	// Create the URL shortener database
	db, err := server.CreateDatabase()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Initialize the short URL generator
	server.InitURLGen()

	// Start the HTTP service with 15-second timeouts
	router := server.NewRouter()
	srv := &http.Server{
		Handler:      router,
		Addr:         serverAddr,
		WriteTimeout: serverTimeoutSecs * time.Second,
		ReadTimeout:  serverTimeoutSecs * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
