package api

// URLMap represents a short-to-long URL mapping
type URLMap struct {
	ShortURL string `json:"shortURL,omitempty"`
	LongURL  string `json:"longURL,omitempty"`
}

// JSONResponse represents the body returned for an API POST request
type JSONResponse struct {
	Code       int    `json:"code"`
	Msg        string `json:"msg"`
	APIVersion string `json:"apiVersion"`
	URLMap     URLMap `json:"urlMap"`
}
